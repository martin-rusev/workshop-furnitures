package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;
import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;
import com.telerikacademy.furniture.models.helper.ValidationClass;

public class ChairImpl extends FurnitureBase implements Chair {

    private int numOfLegs;
    //ChairType chairType;

    public ChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);
        setNumOfLegs(numberOfLegs);
        ValidationClass.validateGreaterThanZero(numberOfLegs);
        //this.chairType = chairType;
    }

//   public ChairType getChairType() {
//        return chairType;
//    }

    private void setNumOfLegs(int numOfLegs) {
        this.numOfLegs = numOfLegs;
    }

    @Override
    public int getNumberOfLegs() {
        return numOfLegs;
    }
    @Override
    public String toString() {
        return String.format( super.toString() +
                "Legs: %d", getNumberOfLegs());

    }
}
