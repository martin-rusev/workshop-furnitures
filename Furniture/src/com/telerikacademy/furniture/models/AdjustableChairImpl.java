package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {

    private ChairType chairType;

    public AdjustableChairImpl(String model, MaterialType materialType, double price, double height, int numOfLegs) {
        super(model, materialType, price, height, numOfLegs);
        this.chairType = ChairType.Adjustable;

    }

    public ChairType getChairType() {
        return chairType;
    }

    @Override
    public void setHeight(double height) {
        super.setHeight(height);
    }
}
