package com.telerikacademy.furniture.models.enums;

public enum ChairType {
    Normal,
    Adjustable,
    Convertible
}
