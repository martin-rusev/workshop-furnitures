package com.telerikacademy.furniture.models.helper;

public class ValidationClass {


    private static final String ARGUMENT_ERROR_MASSAGE = "An argument cannot be null!";
    private static final String EMPTY_ERROR_MASSAGE = "An argument cannot be empty";
    private static final String NO_LEGS_MESSAGE = "An argument cannot be equal or less than zero";

    public static void validateNotNull(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException(ARGUMENT_ERROR_MASSAGE);
        }
    }

    public static void validateNotEmpty(String string) {
        if (string == "") {
            throw new IllegalArgumentException(EMPTY_ERROR_MASSAGE);
        }
    }

    public static void validateGreaterThanZero(int num) {
        if (num <= 0) {
            throw new IllegalArgumentException(NO_LEGS_MESSAGE);
        }
    }
}

