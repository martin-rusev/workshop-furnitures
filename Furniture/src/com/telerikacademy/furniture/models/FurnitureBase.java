package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;
import com.telerikacademy.furniture.models.helper.ValidationClass;

public class FurnitureBase implements Furniture {

    private static final int MODEL_MINIMUM_LENGTH = 3;
    private static final String MODEL_MINIMUM_LENGTH_ERR_MASSAGE = String.format("A model length cannot be less than %d symbols!", MODEL_MINIMUM_LENGTH);
    private static final double PRICE_MINIMUM_VALUE = 0;
    private static final String PRICE_ERROR_MASSAGE = String.format("A price cannot be less than or equal to %.2f", PRICE_MINIMUM_VALUE);
    private static final double HEIGHT_MIN_SIZE = 0;
    private static final String HEIGHT_ERROR_MASSAGE = String.format("A height cannot be less than or equal to %.2f!", HEIGHT_MIN_SIZE);


    private String model;
    private MaterialType materialType;
    private double price;
    protected double height;


    public FurnitureBase(String model, MaterialType materialType, double price, double height) {
        setModel(model);
        this.materialType = materialType;
        setPrice(price);
        setHeight(height);
    }


    private void setModel(String model) {
        ValidationClass.validateNotNull(model);
        ValidationClass.validateNotEmpty(model);
        if (model.length() < MODEL_MINIMUM_LENGTH) {
            throw new IllegalArgumentException(MODEL_MINIMUM_LENGTH_ERR_MASSAGE);
        }
        this.model = model;
    }


    private void setPrice(double price) {
        if (price == 0 || price < 0) {
            throw new IllegalArgumentException(PRICE_ERROR_MASSAGE);
        }
        this.price = price;
    }

    protected void setHeight(double height) {
        if (height == HEIGHT_MIN_SIZE || height < HEIGHT_MIN_SIZE) {
            throw new IllegalArgumentException(HEIGHT_ERROR_MASSAGE);
        }
        this.height = height;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f",
                this.getClass(), getModel(), getMaterialType(),getPrice(),getHeight());
    }

}
