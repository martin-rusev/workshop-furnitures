package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.contracts.Table;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureBase implements Table {


    private static final String LENGTH_ERROR_MASSAGE = "A length must be greater than 0!";
    private static final String WIDTH_ERROR_MASSAGE = "A width must be greater than 0!";
    private double width;
    private double length;
    


    public TableImpl(String model, MaterialType materialType, double price, double height, double length, double width) {
        super(model, materialType, price, height);
        setWidth(width);
        setLength(length);
    }

    private void setLength(double length) {
        if (length <= 0) {
            throw new IllegalArgumentException(LENGTH_ERROR_MASSAGE);
        }
        this.length = length;
    }

    private void setWidth(double width) {
        if (width <= 0) {
            throw new IllegalArgumentException(WIDTH_ERROR_MASSAGE);
        }
        this.width = width;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    @Override
    public String toString() {
        return String.format( super.toString() +
                "Length: %.2f, Width: %.2f, Area: %.4f", getLength(),getWidth(),getArea());
    }
}
