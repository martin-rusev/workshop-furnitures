package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {
    ChairType chairType;
    private boolean isConverted;
    private double defaultHeight;

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numOfLegs) {
        super(model, materialType, price, height, numOfLegs);
        this.chairType = ChairType.Convertible;
        isConverted = false;
        defaultHeight = height;
    }

    @Override
    public boolean getConverted() {
        return isConverted;
    }

    @Override
    public void convert() {
        isConverted = !isConverted;
        if(isConverted){
            setHeight(0.1);
        }else {
            setHeight(defaultHeight);
        }
    }

    @Override
    public String toString() {
        return String.format( super.toString() +
                "State: %s",  isConverted ? "Converted" : "Normal");

    }
}
