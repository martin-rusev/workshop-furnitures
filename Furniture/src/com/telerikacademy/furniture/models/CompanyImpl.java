package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.helper.ValidationClass;

import java.util.ArrayList;
import java.util.List;

public class CompanyImpl implements Company, Comparable {
    private static final int NAME_MINIMUM_LENGTH = 5;
    private static final String NAME_MINIMUM_LENGTH_ERR_MASSAGE = String.format("A name length cannot be less than %d symbols!", NAME_MINIMUM_LENGTH);
    private static final int REGISTRATION_NUMBER_MINIMUM_LENGTH = 10;
    private static final String REGISTRATION_NUMBER_MINIMUM_LENGTH_ERR_MASSAGE = String.format("A registration number must have minimum %d symbols", REGISTRATION_NUMBER_MINIMUM_LENGTH);
    private static final String REGISTRATION_NUMBER_ONLY_NUMBERS_ERR_MASSAGE = "A registration number must contain only numbers!";
    // Finish the class

    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;


    public CompanyImpl(String name, String registrationNumber) {
        super();
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<Furniture>();
    }

    private void setName(String name) {
        ValidationClass.validateNotNull(name);
        ValidationClass.validateNotEmpty(name);
        if (name.length() < NAME_MINIMUM_LENGTH) {
            throw new IllegalArgumentException(NAME_MINIMUM_LENGTH_ERR_MASSAGE);
        }
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber) {
        ValidationClass.validateNotNull(registrationNumber);
        ValidationClass.validateNotEmpty(registrationNumber);
        if (registrationNumber.length() < REGISTRATION_NUMBER_MINIMUM_LENGTH) {
            throw new IllegalArgumentException(REGISTRATION_NUMBER_MINIMUM_LENGTH_ERR_MASSAGE);
        }
        if (!registrationNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException(REGISTRATION_NUMBER_ONLY_NUMBERS_ERR_MASSAGE);
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
            this.registrationNumber = registrationNumber;
    }

    private void setFurnitures(List<Furniture> furnitures) {
        ValidationClass.validateNotNull(furnitures);
        this.furnitures = furnitures;
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {
        return furnitures;
    }

    public void add(Furniture furniture) {

        furnitures.add(furniture);
    }

    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(getName());

        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        ValidationClass.validateNotNull(model);
        for (Furniture furniture : furnitures) {

            if(furniture.getModel() == model){
                return furniture;
            };
        }
        return null;
    }

    public void remove(Furniture furniture) {
        furnitures.remove(furniture);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
