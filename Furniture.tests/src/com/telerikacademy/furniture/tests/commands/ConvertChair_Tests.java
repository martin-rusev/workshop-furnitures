package com.telerikacademy.furniture.tests.commands;

import com.telerikacademy.furniture.commands.ConvertChair;
import com.telerikacademy.furniture.commands.contracts.Command;
import com.telerikacademy.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.furniture.core.factories.FurnitureFactoryImpl;
import com.telerikacademy.furniture.models.CompanyImpl;
import com.telerikacademy.furniture.models.ConvertibleChairImpl;
import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class ConvertChair_Tests {
    private Command testCommand;
    private Furniture testFurniture;
    private FurnitureRepository furnitureRepository;
    private FurnitureFactory furnitureFactory;

    @Before
    public void before() {
        furnitureFactory = new FurnitureFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
        testCommand = new ConvertChair(furnitureRepository, furnitureFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        testCommand.execute(emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        testCommand.execute(asList(new String[2]));
    }

    @Test
    public void execute_should_convertChair_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("model");
        testFurniture = new ConvertibleChairImpl("model", MaterialType.LEATHER, 4, 4, 4);
        furnitureRepository.addFurniture("model", testFurniture);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertTrue(((ConvertibleChair) testFurniture).getConverted());

    }
}
